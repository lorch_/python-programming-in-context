# CHANGELOG #

Changelog for Chapter 1 of Python Programming in Context.

## Changes

| Version | Date | Comment|
| ------ | ------ | ----- |

| 0.2 | 2017-12-01 | - Added exercises 1.8 to 1.16 |

| 0.1 | 2017-11-30 | - Added exercises 1.1 to 1.7 |